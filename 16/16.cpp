﻿#include <windows.h>
#include <stdio.h>
#include <iostream>

int main()
{
    //Get current day
    SYSTEMTIME time;
    GetSystemTime(&time);
    int iTime = time.wDay;

    const int sizeX = 2;
    const int sizeY = 2;
    int arr[sizeX][sizeY];

    for (int i = 0; i < sizeX; i++)
    {
        for (int j = 0; j < sizeY; j++)
        {
            arr[i][j] = i + j;
            std::cout << arr[i][j] << "\n";
        }
    }
    int res = iTime % 5;

    std::cout << "Остаток деления - " << res << std::endl;

    int summaElementov(0);

    for (int t = 0; t < sizeY; t++)
    {
        summaElementov += arr[res][t];
    }

    std::cout << "Сумма элементов строки массива - " << res << " Равна - " << summaElementov << std::endl;
}
